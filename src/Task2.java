import java.math.BigInteger;

public class Task2 {
    public static void main(String[] args) {
        BigInteger result = BigInteger.ONE;

        for (int i = 1; i <= 1000; i++) {
            result = result.multiply(BigInteger.valueOf(i));
        }

        System.out.println(result);
    }
}