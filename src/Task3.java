import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Task3 {
    public static void main(String[] args) {
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
            int firstValue = readIntegerValue(reader, "первое");
            int secondValue = readIntegerValue(reader, "второе");
            int thirdValue = readIntegerValue(reader, "третье");

            int max = firstValue;

            if (secondValue > max) {
                max = secondValue;
            }

            if (thirdValue > max) {
                max = thirdValue;
            }

            System.out.println("Максимальное значение: " + max);

            max = Math.max(firstValue, secondValue);
            max = Math.max(max, thirdValue);

            System.out.println("Максимальное значение с помощью Math.max(): " + max);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static int readIntegerValue(BufferedReader reader, String value) throws IOException {
        while (true) {
            try {
                System.out.print("Введите " + value + " целочисленное значение: ");
                return Integer.parseInt(reader.readLine());
            } catch (NumberFormatException e) {
                System.out.println("Вы ввели неверное целочисленное значение!");
            }
        }
    }
}
