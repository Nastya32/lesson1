import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Task1 {
    public static void main(String[] args) {
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
            String inputValue;

            System.out.print("Введите целочисленное значение: ");

            while (!"exit".equals(inputValue = reader.readLine())) {
                try {
                    int intValue = Integer.parseInt(inputValue);

                    System.out.println(Integer.toBinaryString(intValue));
                    System.out.println(Integer.toOctalString(intValue));
                    System.out.println(Integer.toHexString(intValue));
                } catch (NumberFormatException e) {
                    System.out.println("Вы ввели неверное целочисленное значение!");
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
